# Angular GitLab Pages

In this basic tutorial you will be creating a statically hosted website for your
own domain name using GitLab Pages and Angular Universal.

## Create new GitLab Project

1. Go to https://gitlab.com/projects/new.

2. Make sure to create a project under your own user account.
   `https://gitlab.com/<your_username>/my-awesome-website.tld`

3. Clone the project and initialize the repository:

   ```console
   $ git clone git@gitlab.com:<your_username>/my-awesome-website.tld
   $ cd <my-awesome-website.tld>
   $ touch README.md
   $ git add README.md
   $ git commit -m "add README"
   $ git push -u origin master
   ```

## Host a single `index.html` on GitLab

1. First, create a `public` directory and add a basic HTML document to this
   folder.

   ```console
   $ mkdir -pv public
   $ echo '<html><body><h1>Hello World!</h1></body></html>' >! public/index.html
   ```

2. Create a `.gitlab-ci.yml` and configure it to save the index.html to the job
   artifacts:

   ```console
   $ cat << EOF > .gitlab-ci.yml
   pages:
     stage: deploy
     script:
       - echo "woop woop!"
     artifacts:
       paths:
         - public
     only:
       - master
   EOF
   ```

3. The structure of your repo should look as follows:

   ```console
   $ tree -L 2
   .
   ├── public
   │   └── index.html
   └── README.md
   ```

4. Commit and push:

   ```console
   $ git add -A && git commit -m 'test gitlab pages' && git push
   ```

5. A pipeline should now start running at
   `https://gitlab.com/<your_username>/my-awesome-website.tld/-/pipelines`.

6. Once this job complex, you will see that it completes the "pages" job and the
   "pages:deploy" job.

7. Now, go to "_Settings_" > "_Pages_". On this page you will find the current
   URL of the GitLab Pages location for your new website. Open this link and you
   will see your amazing HTML page.

## Configure Custom Domain for GitLab Pages

1. Under GitLab's Sett, click the "New Domain" button. Here you fill in your
   domain name (of a domain you own or have access to the DNS).

2. Make sure that the _"Automatic certificate management using Let's Encrypt"_
   is enabled, and click "create New Domain".

3. In the next step, you get the DNS records to configure in your ISP's hosting
   console.

4. Go to GitLab _"Settings"_ > _"General"_ >
   _"Visibility, project features, permissions"_, scroll down to _"Pages"_ and
   make sure that visibility is set to _"Everyone"_.

5. After all this configuration is done, it will take a little while before all
   of this validates. In the meanwhile we can start building our Angular
   application.

## Create new Angular Workspace

1. Install latest version of Angular CLI globally:

   ```console
   $ npm install -g @angular/cli@latest
   ```

2. Create a new Angular workspace:

   ```console
   $ ng new my-site \
      --create-application "false" \
      --prefix "app" \
      --strict "true" \
      --style "scss" \
      --minimal "true"
   ```

3. Go to the directory where the workspace was created:

   ```console
   $ cd my-site
   ```

4. Create a new Angular application inside the workspace:

   ```console
   $ ng g application my-site \
      --legacy-browsers "false" \
      --style "scss" \
      --strict "true" \
      --routing "true" \
      --prefix "app" \
      --minimal "true"
   ```

5. Start your application:

   ```console
   $ npm start
   ```

6. Open your browser at [`http://localhost:4200`](http://localhost:4200).

7. You should now be see the default page. Let's add a routes to this site:

   ```console
   $ ng generate module home --route home --module app.module
   ```

   ```console
   $ ng generate module about --route about --module app.module
   ```

8. Edit `app.component.ts`:

   ```ts
   @Component({
     selector: "app-root",
     template: `
       <nav>
         <ul>
           <li *ngFor="let link of links">
             <a [routerLink]="link.url">{{ link.text }}</a>
           </li>
         </ul>
       </nav>
       <router-outlet></router-outlet>
     `,
     styles: [],
   })
   export class AppComponent {
     links = [
       { url: "home", text: "Home" },
       { url: "about", text: "About" },
     ];
   }
   ```

9. Edit `app-routing.ts` and add a default redirect to the home page:

   ```ts
   const routes: Routes = [
     {
       path: "home",
       loadChildren: () =>
         import("./home/home.module").then((m) => m.HomeModule),
     },
     {
       path: "about",
       loadChildren: () =>
         import("./about/about.module").then((m) => m.AboutModule),
     },
     {
       path: "",
       pathMatch: "full",
       redirectTo: "home",
     },
   ];
   ```

10. Now when you open [localhost:4200](http://localhost:4200), you will
    automatically be redirected to (http://localhost:4200/home).

## Add Angular Universal

1. Add Angular Universal

   ```console
   $ ng add @nguniversal/express-engine
   ```

2. Add `angular-prerender`

   ```console
   $ npm install angular-prerender --save-dev
   ```

## Pre-render

Now, let's test and see how everything is working to generate static HTML for
our Angular site.

1. Build the application:

   ```console
   $ ng build
   ```

2. Generate the server bundle:

   ```console
   $ ng run my-site:server
   ```

3. Pre-render the site:

   ```console
   $ npx angular-prerender
   ```

4. Test the results with a local webserver:

   ```console
   $ npx angular-prerender dist/my-site/browser
   ```

   See the site in action on [localhost:8080](http://localhost:8080).

## Deploy to GitLab Pages

Now, we're going to tell GitLab how to build our Angular application and upload
the results to GitLab Pages.

1. Update the contents of `.gitlab-ci.yml`:

   ```yaml
   pages:
     image: node:14
     stage: deploy
     script:
       - npm ci
       - npx ng build
       - npx ng run my-site:server
       - npx angular-prerender
       - rm -rf public/
       - cp -Rv my-site/dist/my-site/browser public/
     artifacts:
       paths:
         - public
     only:
       - master
   ```

Congratulations! You're now running your pre-rendered Angular site on your
custom domain, using GitLab Pages.

You can inspect the content of the site, by viewing the page source or `curl`:

```console
$ npm i -g html-cli
```

```console
$ curl -sL https://angular-gitlab-pages.felix.dulfer.dev/about | html
...
        <nav>
            <ul>
                <li><a href="/home">Home</a></li>
                <li><a href="/about">About</a></li>
                <!---->
            </ul>
        </nav>
        <router-outlet></router-outlet>
        <app-about>
            <p> about works! </p>
        </app-about>
...
```

As you can see, we're serving up the server-side rendered HTML.

## Improvements

<!-- ### Replace GitLab's default 404 page

The 404 page that GitLab provides may not be what you want. To replace this,
we'll use our own index.html to add a 404.html.

1. Update the contents of `.gitlab-ci.yml`:

   ```yaml
   pages:
     image: node:14
     stage: deploy
     script:
       - npm ci
       - npx ng build
       - npx ng run my-site:server
       - npx angular-prerender
       - rm -rf public/
       - cp -Rv my-site/dist/my-site/browser public/
       - cp public/index.html public/404.html
     artifacts:
       paths:
         - public
     only:
       - master
   ``` -->

### Caching GitLab CI/CD

```yaml
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .npm/

before_script:
  - cd my-site
  - npm ci --cache .npm --prefer-offline

pages:
  image: node:14
  stage: deploy
  script:
    - cd my-site
    - npx ng build
    - npx ng run my-site:server
    - npx angular-prerender
    - rm -rf public/
    - cp -Rv dist/my-site/browser public/
    - cp public/index.html public/404.html
  artifacts:
    paths:
      - public
  only:
    - master
```

### Set app title

By default, the title of each page will be whatever you have set in index.html.
But if you want it to show something like "My Site | About" for the about page,
you'll need to set this for each page:

https://angular.io/guide/set-document-title
