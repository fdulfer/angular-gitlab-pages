import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <nav>
      <ul>
        <li *ngFor="let link of links">
          <a [routerLink]="link.url">{{ link.text }}</a>
        </li>
      </ul>
    </nav>
    <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {
  links = [
    { url: "home", text: "Home" },
    { url: "about", text: "About" },
  ];
}
